#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash ~/NewSonyMenu/res/models/2015/z5dflash.sh &)
else
	command=$(rm -r ~/local/*)
	zenity --error --text="Temp Files Cleared"
	exit
fi

/bin/bash ~/NewSonyMenu/res/models/2015/z5pagain.sh

sleep 5
