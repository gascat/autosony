#!/bin/bash

#create files
touch modtemp
touch modtemp2
touch ~/autotest/"$(date +"%Y_%m_%d").log"

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	zenity --info --text="Please connect the device in Fastboot mode (Hold VOL+ while connecting)"
	#get product as variable
	fastboot getvar product 2>&1 | tee modtemp2
	model=$(awk 'NR==1 {print $2}' modtemp2)

	#get serial no as variable
	fastboot getvar serialno 2>&1 | tee modtemp
	serial=$(awk 'NR==1 {print $2}' modtemp)

	#echo data to log file
	echo $USER >> "$(date +"%Y_%m_%d").log"
	echo $model >> "$(date +"%Y_%m_%d").log"
	echo $serial >> "$(date +"%Y_%m_%d").log"
	echo $(date "+%T") >> "$(date +"%Y_%m_%d").log"
	zenity --info --text "Please hold VOL- and press OK"
	command=$(/bin/bash ~/autotest/models/2017/xa1flash.sh &)
	rm -r ~/autotest/modtemp
	rm -r ~/autotest/modtemp2
else
	command=$(rm -r ~/local/*)
	zenity --error --text="Temp Files Cleared"
	exit
fi

/bin/bash ~/autotest/models/2017/xa1again.sh

sleep 5
