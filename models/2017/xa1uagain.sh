#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash ~/NewSonyMenu/res/models/2017/xa1uflash.sh &)
else
	command=$(rm -r ~/local/*)
	zenity --error --text="Temp Files Cleared"
	exit
fi

/bin/bash ~/NewSonyMenu/res/models/2017/xa1uagain.sh

sleep 5
