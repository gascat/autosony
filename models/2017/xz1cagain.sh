#!/bin/bash

zenity --question --text="Flash Again?" --ok-label="Yes" --cancel-label="No"
if [ $? = 0 ]; then
	command=$(/bin/bash ~/NewSonyMenu/res/models/2017/xz1cflash.sh &)
else
	command=$(rm -r ~/local/*)
	zenity --error --text="Temp Files Cleared"
	exit
fi

/bin/bash ~/NewSonyMenu/res/models/2017/xz1cagain.sh

sleep 5
