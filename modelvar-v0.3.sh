#!/bin/bash

#create files
touch modtemp
touch modtemp2
touch ~/autotest/logs/"$(date +"%Y_%m_%d").log"

zenity --info --text="please connect the device in Fastboot mode. Hold VOL+ (Up) while connecting"

#get product as variable
fastboot getvar product 2>&1 | tee modtemp2
model=$(awk 'NR==1 {print $2}' modtemp2)

#get serial no as variable
fastboot getvar serialno 2>&1 | tee modtemp
serial=$(awk 'NR==1 {print $2}' modtemp)

#echo data to log file
echo $USER >> ~/autotest/logs/"$(date +"%Y_%m_%d").log"
echo $model >> ~/autotest/logs/"$(date +"%Y_%m_%d").log"
echo $serial >> ~/autotest/logs/"$(date +"%Y_%m_%d").log"
echo $(date "+%T") >> ~/autotest/logs/"$(date +"%Y_%m_%d").log"

#notify user for flash mode
zenity --info --text="Device Info Collected. Please hold VOL- (Down) and press OK"

#reboot into flash
#fastboot reboot-bootloader

#remove temp files
rm -r ~/autotest/modtemp
rm -r ~/autotest/modtemp2

#create log
#touch ~/autotest/logs/temp.log

#run device flash script
sudo ~/autotest/models/$model.sh



